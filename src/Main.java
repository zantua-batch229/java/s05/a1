import com.zuitt.phonebook.Contact;
import com.zuitt.phonebook.Phonebook;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        ArrayList<Contact> contacts = new ArrayList();
        Phonebook phonebook = new Phonebook(contacts);
        Contact person1 = new Contact("John Doe", "my home in Quezon City", "+639152468596");
        Contact person2 = new Contact("Jane Doe", "my home in Caloocan City", "+639162148573");
        contacts.add(person1);
        contacts.add(person2);
        ArrayList<Contact> allContacts = phonebook.getContacts();
        if (allContacts.isEmpty()) {
            System.out.println("The phonebook is empty");
        } else {
            for(int x = 0; x < allContacts.toArray().length; x++  ){
                System.out.println("-------------------");
                System.out.println(allContacts.get(x).getName());
                System.out.println("-------------------");
                System.out.println(allContacts.get(x).getName() + " has the following registered number:");
                System.out.println(allContacts.get(x).getContactNumber());
                System.out.println(allContacts.get(x).getName() + " has the following registered address:");
                System.out.println(allContacts.get(x).getAddress());

            }




        }



    }
}