package com.zuitt.phonebook;

import java.util.ArrayList;

public class Phonebook {
    private Contact contact;
    private ArrayList<Contact> contacts;

    public Phonebook(){}

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;

    }

    public void setContact(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }
}
